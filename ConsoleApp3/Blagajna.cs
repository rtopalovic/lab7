﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3 {
    class Blagajna {

        public Blagajna() {
            racuni = new List<IRacun>();
        }

        public void dodajIRacun(IRacun racun) {
            racuni.Add(racun);
        }

        public void prikaziRacune() {
            foreach (IRacun racun in racuni) {
                Console.WriteLine(racun.DohvatiIznos());
                Console.WriteLine(racun.DohvatiDatumIzdavanja());
            }
        }

        List<IRacun> racuni;
    }
}
