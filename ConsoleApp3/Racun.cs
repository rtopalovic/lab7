﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3 {
    class Racun<T> : IRacun {

        //Constructori
        public Racun(T iznos) {
            iznosRacuna = iznos;
            datumIzdavanja = DateTime.UtcNow;
        }

        public decimal DohvatiIznos(){
            return Convert.ToDecimal(IznosRacuna);
        }

        public DateTime DohvatiDatumIzdavanja() {
            return datumIzdavanja;
        }

        //Properties
        public T IznosRacuna {
            get {
                return iznosRacuna;
            }
            set {
                if (Convert.ToDouble(value) < 10) {
                    throw new IznimkaBlagajne("Premali iznos!");
                } else {
                    iznosRacuna = value;
                    datumIzdavanja = DateTime.UtcNow;
                }
            }
        }

        //Podatkovni clanovi
        public T iznosRacuna;
        private DateTime datumIzdavanja;
    }
}
