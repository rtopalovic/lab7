﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3 {
    class IznimkaBlagajne : Exception {
        public IznimkaBlagajne(String parametar) : base(parametar) { }
    }
}
